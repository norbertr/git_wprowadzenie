import pandas as pd

if __name__ == '__main__':
    data = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/tips.csv')

    data_aggregated = data.groupby("day")["tip"].mean()
    data_aggregated.to_csv("data_aggregated.csv")



